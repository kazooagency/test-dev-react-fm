import * as Yup from "yup";

import { elementsMustMatch, Required } from "./CustomValidationMessages";

const PasswordConfirm = ({ passwordRef } = { passwordRef: "password" }) =>
  Yup.string()
    .oneOf(
      [Yup.ref(passwordRef), null],
      elementsMustMatch({ elements: "Passwords" })
    )
    .required(Required);

export default PasswordConfirm;
