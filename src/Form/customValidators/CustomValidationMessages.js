export const InvalidElement = ({ element }) =>
  `You have to provide a valid ${element}.`;
export const Required = "Required";

export const minMaxMessage = ({ min, max, element }) =>
  `Your ${element} must be between ${min} and ${max} characters length.`;

export const elementsMustMatch = ({ elements }) => `${elements} must match.`;
