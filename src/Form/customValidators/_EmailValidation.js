import * as Yup from "yup";
import { InvalidElement, Required } from "./CustomValidationMessages";

const EmailValidation = () =>
  Yup.string()
    .email(InvalidElement({ element: "email" }))
    .required(Required);

export default EmailValidation;
