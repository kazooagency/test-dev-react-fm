import * as Yup from "yup";
import { Required, minMaxMessage } from "./CustomValidationMessages";

const PasswordValidation = ({ min, max } = { min: 5, max: 16 }) => {
  const minMaxPassword = minMaxMessage({
    min,
    max,
    element: "password"
  });
  return Yup.string()
    .min(min, minMaxPassword)
    .max(max, minMaxPassword)
    .required(Required);
};

export default PasswordValidation;
