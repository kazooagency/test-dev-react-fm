import EmailValidation from "./_EmailValidation";
import PasswordValidation from "./_PasswordValidation";
import PasswordConfirmValidation from "./_PasswordConfirmValidation";

const Validators = {
  email: EmailValidation,
  password: PasswordValidation,
  confirm: PasswordConfirmValidation
};

export default Validators;
