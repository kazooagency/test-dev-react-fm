import React from "react";
import { Formik, Form } from "formik";

const FormWithSubmit = ({
  initialValues,
  validationSchema,
  onSubmit,
  submitLabel,
  forminputs
}) => {
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => onSubmit(values)}
      >
        <Form className="Form">
          {forminputs}
          <button type="submit">{submitLabel}</button>
        </Form>
      </Formik>
    </>
  );
};

export default FormWithSubmit;
