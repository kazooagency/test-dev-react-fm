import React from "react";
import { useField } from "formik";

const CustomTextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div
      className={`form-item ${meta.touched && meta.error ? "has-error" : ""}`}
    >
      <label htmlFor={props.id || props.name}>{label}</label>
      <input {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="error-message">
          <span className="error-item">{meta.error}</span>
        </div>
      ) : null}
    </div>
  );
};

export default CustomTextInput;
