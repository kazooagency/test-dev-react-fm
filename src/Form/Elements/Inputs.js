import React from "react";
import CustomTextInput from "./CustomTextInput";

export const EmailInput = ({ name }) => (
  <CustomTextInput label="Email" name={name} type="email" placeholder="" />
);

export const PasswordConfirmInput = ({ name }) => (
  <CustomTextInput
    label="Confirmer mot de passe"
    name={name}
    type="password"
    placeholder=""
  />
);

export const PasswordInput = ({ name }) => (
  <CustomTextInput
    label="Mot de passe"
    name={name}
    type="password"
    placeholder=""
  />
);
