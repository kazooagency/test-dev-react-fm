import React from "react";
import { render, wait, fireEvent } from "@testing-library/react";
import RegistrationForm from "./RegistrationForm";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
  const div = document.createElement("div");
  render(<RegistrationForm />, div);
});

it("submits correct values", async () => {
  const original_alert = window.alert;
  window.alert = jest.fn();
  const { container } = render(<RegistrationForm />);
  const email = container.querySelector('input[name="email"]');
  const password = container.querySelector('input[name="password"]');
  const confirm = container.querySelector('input[name="confirm"]');
  const submit = container.querySelector('button[type="submit"]');

  await wait(() => {
    fireEvent.change(email, {
      target: {
        value: "mock@email.com"
      }
    });
  });

  await wait(() => {
    fireEvent.change(password, {
      target: {
        value: "testpass"
      }
    });
  });

  await wait(() => {
    fireEvent.change(confirm, {
      target: {
        value: "testpass"
      }
    });
  });

  await wait(() => {
    fireEvent.click(submit);
  });

  expect(window.alert).toHaveBeenCalledWith(
    '{"email":"mock@email.com","password":"testpass","confirm":"testpass"}'
  );
  window.alert = original_alert;
});

it("does not submit with incorrect values", async () => {
  const original_alert = window.alert;
  window.alert = jest.fn();
  const { container } = render(<RegistrationForm />);
  const email = container.querySelector('input[name="email"]');
  const password = container.querySelector('input[name="password"]');
  const confirm = container.querySelector('input[name="confirm"]');
  const submit = container.querySelector('button[type="submit"]');

  await wait(() => {
    fireEvent.change(email, {
      target: {
        value: "mock@email.com"
      }
    });
  });

  await wait(() => {
    fireEvent.change(password, {
      target: {
        value: "testpass"
      }
    });
  });

  await wait(() => {
    fireEvent.change(confirm, {
      target: {
        value: "testpassDifferent"
      }
    });
  });

  await wait(() => {
    fireEvent.click(submit);
  });

  expect(window.alert).not.toHaveBeenCalled();
  window.alert = original_alert;
});
