import React from "react";
import * as Yup from "yup";

import Validators from "../../customValidators/Validators";

import {
  EmailInput,
  PasswordInput,
  PasswordConfirmInput
} from "../../Elements/Inputs";

import FormWithSubmit from "../../FormTemplate/FormWithSubmit";

const formInputsNames = {
  email: "email",
  password: "password",
  confirm: "confirm"
};

const formInputs = (
  <>
    <EmailInput name={formInputsNames.email} />
    <PasswordInput name={formInputsNames.password} />
    <PasswordConfirmInput name={formInputsNames.confirm} />
  </>
);

const initialValues = {
  [formInputsNames.email]: "",
  [formInputsNames.password]: "",
  [formInputsNames.confirm]: ""
};

const validationSchema = Yup.object({
  [formInputsNames.email]: Validators.email(),
  [formInputsNames.password]: Validators.password(),
  [formInputsNames.confirm]: Validators.confirm()
});

const onSubmit = values => {
  alert(JSON.stringify(values));
};

const RegistrationForm = () => {
  return (
    <FormWithSubmit
      forminputs={formInputs}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      submitLabel={"M'inscrire"}
    />
  );
};

export default RegistrationForm;
