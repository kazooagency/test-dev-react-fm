import React from "react";
import * as Yup from "yup";

import Validators from "../../customValidators/Validators";

import { EmailInput, PasswordInput } from "../../Elements/Inputs";

import FormWithSubmit from "../../FormTemplate/FormWithSubmit";

const formInputsNames = {
  email: "email",
  password: "password"
};

const formInputs = (
  <>
    <EmailInput name={formInputsNames.email} />
    <PasswordInput name={formInputsNames.password} />
  </>
);

const initialValues = {
  [formInputsNames.email]: "",
  [formInputsNames.password]: ""
};

const validationSchema = Yup.object({
  [formInputsNames.email]: Validators.email(),
  [formInputsNames.password]: Validators.password()
});

const onSubmit = values => {
  alert(JSON.stringify(values));
};

const LoginForm = () => {
  return (
    <FormWithSubmit
      forminputs={formInputs}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      submitLabel={"Me connecter"}
    />
  );
};

export default LoginForm;
