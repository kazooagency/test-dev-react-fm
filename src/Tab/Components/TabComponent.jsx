import React from "react";
import TabHeader from "../Elements/TabHeader";
import TabContainer from "../Elements/TabContainer";
import TabContent from "../Elements/TabContent";

import RegistrationForm from "../../Form/Components/RegisterForm/RegistrationForm";
import LoginForm from "../../Form/Components/LoginForm/LoginForm";

import { LOGIN, REGISTER } from "../Elements/HeaderValues";

const TabComponent = () => {
  return (
    <TabContainer>
      <TabHeader filter={LOGIN}>J'ai un compte</TabHeader>
      <TabHeader filter={REGISTER}>Je n'ai pas de compte</TabHeader>
      <TabContent filter={LOGIN}>
        <LoginForm />
      </TabContent>
      <TabContent filter={REGISTER}>
        <RegistrationForm />
      </TabContent>
    </TabContainer>
  );
};

export default TabComponent;
