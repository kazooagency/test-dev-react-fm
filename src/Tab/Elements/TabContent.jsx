import React from "react";

const TabContent = ({ children }) => (
  <div className="tab-content">{children}</div>
);

export default TabContent;
