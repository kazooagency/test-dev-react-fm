import React from "react";

const MenuItem = ({ className, handleOnclick, title }) => {
  return (
    <button className={className} onClick={handleOnclick}>
      {title}
    </button>
  );
};

export default MenuItem;
