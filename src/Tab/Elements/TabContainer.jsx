import React, { useState } from "react";
import MenuItem from "./MenuItem";

import { LOGIN } from "./HeaderValues";

const TabContainer = ({ children }) => {
  const [category, setCategory] = useState(LOGIN);

  const header = children.filter(child => child.type.name === "TabHeader");
  const content = children.filter(
    child => child.type.name === "TabContent" && child.props.filter === category
  );

  return (
    <div className="tab-container">
      <div className="tab-header">
        {header.map(el => (
          <MenuItem
            handleOnclick={() => setCategory(el.props.filter)}
            title={el.props.children}
            className={`menu-item ${
              el.props.filter === category ? "is-active" : ""
            }`}
            key={el.props.children}
          />
        ))}
      </div>
      {content.map(el => el)}
    </div>
  );
};

export default TabContainer;
