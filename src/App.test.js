import React from "react";
import { render } from "@testing-library/react";
import App from "./App";
import "@testing-library/jest-dom/extend-expect";

window.alert = jest.fn();

it("renders without crashing", () => {
  const div = document.createElement("div");
  render(<App />, div);
});
