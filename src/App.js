import React from "react";
import TabComponent from "./Tab/Components/TabComponent";
import "./App.css";

function App() {
  return (
    <div className="App">
      <TabComponent />
    </div>
  );
}

export default App;
